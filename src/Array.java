import java.util.Scanner;

//public class Array {
//    public static void main(String[] args) {
//        int[] emptyArray = new int[5]; // массив на 5 элементов не указанных
//        int[] fullArray = {5, 15, 222, 345, 522}; // массив с указанными элементами
//        emptyArray[4] = 10; // присваиваем ячейке 5 значение 10
//        String[] names = {"Roman", "Igor", "Vasya", "Alena", "Jack"};
//        int x = fullArray[0] + fullArray[3];
//        String[][] nameOfLeaders = {
//                {"Peter", "Ahmed", "Ramzes"}, {" The First", " The Great", " The Glorious"} // массив массивов
//        };
//        System.out.println(names[0] + " has " + fullArray[0] + " points");
//        System.out.println(nameOfLeaders[0][0] + nameOfLeaders[1][0]); // Номер массива, номер ячейки + то же самое
//        System.out.println(nameOfLeaders[0][1] + nameOfLeaders[1][1]);
//        System.out.println(nameOfLeaders[0][2] + nameOfLeaders[1][2]);
//        }
//}

//**********************************************************************


public class Array {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //Перепенная сканнер - ввод из строки
        System.out.print("Choose TV-channel 1-5,\n 0 to Exit: "); // строка для ввода сканера
        int x = scanner.nextInt();
        String[] channelNames = new String[6];
        channelNames[1]  = "ORT";
        channelNames[2]  = "TVRus";
        channelNames[3]  = "RTR";
        channelNames[4]  = "NTV";
        channelNames[5]  = "Ren-TV";
        if (x == 0) {
            System.out.println("Good Bye!");
        } else if (x == 1) {
            System.out.println(channelNames[1]);
        } else if (x == 2) {
            System.out.println(channelNames[2]);
        } else if (x == 3) {
            System.out.println(channelNames[3]);
        } else if (x == 4) {
            System.out.println(channelNames[4]);
        } else if (x == 5) {
            System.out.println(channelNames[5]);
        } else if (x >= 6) {
            System.out.println("Choose channels from 1 to 5");
        }
    }
}