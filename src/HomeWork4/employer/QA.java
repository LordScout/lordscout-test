package HomeWork4.employer;

import HomeWork4.LevelKnowledge;

public class QA extends Employer {

    public QA(LevelKnowledge level) {
        super(level);
    }

    @Override
    public void doWork() {
        System.out.println("QA");
        switch (getLevel()) {
            case JUNIOR: {
                test();
                makeTestCase();
                break;
            }
            case MIDDLE: {
                test();
                makeTestCase();
                develop();
                break;
            }
            case SENIOR: {
                test();
                develop();
                teach();
                break;
            }
            case TEAMLEAD:
                teach();
                break;
            default:
                System.out.println("Уровень специалиста не известен");
        }


    }

    private void develop() {
        System.out.println("Выкатываю в прод");
    }

    private void test() {
        System.out.println("Тестирую");
    }

    private void makeTestCase() {
        System.out.println("Пишу тест кейсы");
    }

    private void teach() {
        System.out.println("Учу");
    }

}

