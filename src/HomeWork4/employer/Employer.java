package HomeWork4.employer;

import HomeWork4.LevelKnowledge;

public abstract class Employer {

    private LevelKnowledge level = null;

    public Employer(LevelKnowledge level) {
        this.level = level;
    }

    public LevelKnowledge getLevel() {
        return level;
    }

    public abstract void doWork();

    @Override
    public String toString() {
        return getLevel().getValue();
    }

}
