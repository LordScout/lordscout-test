package HomeWork4.employer;

import HomeWork4.LevelKnowledge;

public class AQA extends Employer {

    public AQA(LevelKnowledge level) {
        super(level);
    }

    @Override
    public void doWork() {
        System.out.println("AQA");
        switch (getLevel()) {
            case JUNIOR: {
                makeAutotest();
                makeTestCase();
                break;
            }
            case MIDDLE: {
                makeAutotest();
                makeTestCase();
                codeReview();
                break;
            }
            case SENIOR: {
                makeAutotest();
                codeReview();
                teach();
                break;
            }
            case TEAMLEAD:
                teach();
                break;
            default:
                System.out.println("Уровень специалиста не известен");
        }

    }

    private void makeAutotest(){System.out.println("Автотесты");}
    private void makeTestCase(){System.out.println("Пишу тест кейсы");}
    private void codeReview(){System.out.println("Код контроль");}
    private void teach(){System.out.println("Учу");}


}
