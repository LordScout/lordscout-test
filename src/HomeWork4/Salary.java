package HomeWork4;

import HomeWork4.employer.Employer;

import java.util.HashMap;


public abstract class Salary {
    public Salary() {
        this.grade = new HashMap<>();
        grade.put(LevelKnowledge.JUNIOR, 1.45f);
        grade.put(LevelKnowledge.MIDDLE, 2.45f);
        grade.put(LevelKnowledge.SENIOR, 3.45f);
        grade.put(LevelKnowledge.TEAMLEAD, 5.12f);
    }

    protected HashMap<LevelKnowledge, Float> grade ;
    public float koeff = 1.25f;
    public static String staticVariable = "dsfsdfdsfsdf";

    public float calculateSalary(Employer employer){
        float grade = this.grade.get(employer.getLevel());
        float salary = grade * koeff * 50000;
        return salary;
    }
}

