package HomeWork4;

import HomeWork4.employer.AQA;
import HomeWork4.employer.Employer;
import HomeWork4.employer.QA;

import java.util.*;


public class DataGenerator extends Salary {

    public static ArrayList<Employer> generateEmployers() {
        int sizeArray = 20;
        ArrayList<Employer> stuff = new ArrayList<>();
        for (int i = 0; i < sizeArray; i++) {
            stuff.add(new QA(LevelKnowledge.getRandomKnowledge()));
            stuff.add(new AQA(LevelKnowledge.getRandomKnowledge()));
        }
        return stuff;
    }
}