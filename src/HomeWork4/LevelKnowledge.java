package HomeWork4;

import java.util.Random;

public enum LevelKnowledge {
    JUNIOR("специалист"),
    MIDDLE("старший специалист"),
    SENIOR("ведущий специалист"),

    TEAMLEAD("руководитель отдела");

    LevelKnowledge(String value) {

        this.value = value;
    }

    private final String value;

    public String getValue() {
        return value;
    }
    public static LevelKnowledge getRandomKnowledge() {
        return values()[new Random().nextInt(values().length)];
    }
}

