import java.util.Scanner;

public class While {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //Перепенная сканнер - ввод из строки
        System.out.print("Insert number: "); // строка для ввода сканера
        int x = scanner.nextInt(); // х - присвоили то, что ввели
        int sum = 0; // стартовая сумма 0
        if (Math.signum(x) == 1.0F) { // Проверяем знак введённого числа. Если минус, то будет -1.0F
            for (int i = 0; i != x; i++) {  // Цикл. Старт с 0, пока меньше или равно х, прибавляем 1

                sum += i;   // к сумме плюсуем значение
            }
        }
            else {  // Если минус был, то отнимаем
                for (int i = 0; i != x; i--) {  // Цикл. Старт с 0, пока меньше или равно х, прибавляем 1

                    sum += i;
            }
        }
        System.out.printf("Sum numbers from 0 to %d is %d", x, sum); //

//**********************************************************************

//        String str = new String();
//        Scanner scanner = new Scanner(System.in);
//        String exit = "Exit"; // присвоили текст переменной, чтобы можно было сравнивать со строкой
//        while (!exit.equals(str)) { // сравниваем реоеменную, не равную тексту!
//            System.out.print("Write something: "); // просим ввести текст
//            str = scanner.nextLine(); // сканируем ввод и возвращаем в начало цикла
//        }

    }
}
