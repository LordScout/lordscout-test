import java.util.Scanner;

//public class IfElse {
//    public static void main(String[] args) {
//        System.out.print("Enter your score: ");
//        int score = new Scanner(System.in).nextInt();
//        if (score > 100 || score < 0 ) {
//            System.out.println("Wrong input value");
//        } else {
//            if (score >= 90) {
//                System.out.println("You are best with score " + score);
//            } else if (score >= 80) {
//                System.out.println("Very good, your score " + score);
//            } else if (score >= 70) {
//                System.out.println("Good job with score " + score);
//            } else if (score >= 60) {
//                System.out.println("Not bad, but you can do more than " + score);
//            } else if (score >= 50) {
//                System.out.println("Poor score! " + score + " is very low");
//            } else {
//                System.out.println("Did you even tried with your " + score + " points");
//            }
//        }

//**********************************************************************

//        System.out.print("Enter your score: ");
//        int score = new Scanner(System.in).nextInt();
//        String message;
//
//        message = score >= 70 ? "Congrats!" : "You are done with that"; // Тернарный оператор. Если то 1, нет - 2.
//        System.out.println(message);

//**********************************************************************

//        System.out.print("Enter your score: ");
//        int score = new Scanner(System.in).nextInt();
//
//        switch (score) { // оператор переключения, подбора (в скобках нужное значение)
//            case 1: // 1 - цифра, которую ожидаем в строке ввода
//                System.out.println("one"); // ответ, если совпало
//                break;
//            case 2:
//                System.out.println("two");
//                break;
//            default:
//                System.out.println("You missed");
//                break;
//        }

//**********************************************************************


public class IfElse {
    public static void main(String[] args) {
        int menuChoose;                                         // Инициализировали переменную для счётчика ввода
        String name = "";                                       // Инициализировали переменную для вывода имени
        String surName = "";                                    // Инициализировали переменную для вывода фамилии
        String birthYear = "";                                  // Инициализировали переменную для вывода года рождения
        do {                                                    // Открыли цикл
            System.out.print("* 1. Ввести имя\n" +              // Выводим список вариантов для ввода из строки
                    "* 2. Ввести фамилию\n" +
                    "* 3. Ввести год рождения\n" +
                    "* 4. Вывести информацию\n" +
                    "* 0. Выход\n" +
                    "Введите номер меню : ");
            Scanner scanner = new Scanner(System.in);           // Инициализировали сканнер с переменной scanner
            menuChoose = scanner.nextInt();                     // Открыли сканнер с инициализированной переменной

            switch (menuChoose) {                               // Включили тумблер, слушающий ввод
                case 1:
                    System.out.println("Write your name: ");    // Печатаем надстроку для ввода
                    Scanner Name = new Scanner(System.in);      // Инициализировали сканнер с переменной name
                    name = Name.nextLine();                     // Открыли сканнер с инициализированной переменной
                    System.out.println("Your Name is: " + name);// Вывели сообщения о принятии переменной
                    break;                                      // Вышли из свича
                case 2:
                    System.out.println("Write your Surname: ");
                    Scanner SurName = new Scanner(System.in);
                    surName = SurName.nextLine();
                    System.out.println("Your Surname is: " + surName);
                    break;
                case 3:
                    System.out.println("Write your Year of Birth: ");
                    Scanner BirthYear = new Scanner(System.in);
                    birthYear = BirthYear.nextLine();
                    System.out.println("Your Year of Birth is: " + birthYear);
                    break;
                case 4:
                    System.out.println(name + " " + surName + " " + birthYear); // Слепили в выдачу все переменные
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Choose 1-4 or 0 for Exit"); // Сообщение при прочем вводе
                    break;
            }
        } while (menuChoose != 0);                              // Условие для выхода из цикла
        System.out.println("Good bye!");                        // Сообщение при выходе из программы
    }
}

//**********************************************************************
//
//public class IfElse {
//    public static void main(String[] args) {
//        System.out.print("Enter your scores: ");
//        int score1 = new Scanner(System.in).nextInt();
//        System.out.print("Enter one more: ");
//        int score2 = new Scanner(System.in).nextInt();
//        System.out.print("Enter one more again: ");
//        int score3 = new Scanner(System.in).nextInt();
//        System.out.print("Enter last one: ");
//        int score4 = new Scanner(System.in).nextInt();
//        int positiveNumber = 0;
//        int negativeNumber = 0;
//        if (Math.signum(score1) == 1.0F) {                  // Считаем количество введённых плюс и минус чисел
//            positiveNumber = positiveNumber + 1;
//        } else { negativeNumber = negativeNumber +1; }
//        if (Math.signum(score2) == 1.0F) {
//            positiveNumber = positiveNumber + 1;
//        } else { negativeNumber = negativeNumber +1; }
//        if (Math.signum(score3) == 1.0F) {
//            positiveNumber = positiveNumber + 1;
//        } else { negativeNumber = negativeNumber +1; }
//        if (Math.signum(score4) == 1.0F) {
//            positiveNumber = positiveNumber + 1;
//        } else { negativeNumber = negativeNumber +1; }
//        System.out.println("Positives: " + positiveNumber + " " + "Negatives: " + negativeNumber);
//        }
//    }

//**********************************************************************

//public class IfElse {
//    public static void main(String[] args) {
//        System.out.print("Enter your scores: ");
//        int score1 = new Scanner(System.in).nextInt();
//        System.out.print("Enter one more: ");
//        int score2 = new Scanner(System.in).nextInt();
//        System.out.print("Enter one more again: ");
//        int score3 = new Scanner(System.in).nextInt();
//        int min;
//        int max;
//
//        if (score1 < score2) {
//            min = score1;
//        } else {
//            min = score2;
//        }
//
//        if (min > score3) {
//            min = score3;
//        }
//
//        if (score1 > score2) {
//            max = score1;
//        } else {
//            max = score2;
//        }
//
//        if (max < score3) {
//            max = score3;
//        }
//        System.out.println(max + min);  // Вычисляем сумму минимального и макс-ного числа из 3х
//    }
//}

//**********************************************************************