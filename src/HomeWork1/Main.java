package HomeWork1;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;
import java.time.Period;


public class Main {
    public static void main(String[] args) throws ParseException {
        String bob = "Bob";
        LocalDate dateToday = LocalDate.now();
        Scanner sc = new Scanner(System.in);
        System.out.print("Input your name: ");
        String name = sc.nextLine();

        if (name.equalsIgnoreCase(bob)) {
            System.out.println(" Enter your date of birth in following format : dd-mm-yyyy");
            String inputDate = sc.nextLine();
            DateTimeFormatter formattedDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate birthDate = LocalDate.parse(inputDate, formattedDate);
            Period period = Period.between(birthDate, dateToday);
            if (period.getYears() >= 18) {
                long days = ChronoUnit.DAYS.between(birthDate, dateToday);
                System.out.println("Your credit sum is " + days * 2);
            } else {
                System.out.println("Credit denied!");
            }
        } else {
            System.out.print("Input your age: ");
            short age = sc.nextShort();
            System.out.print("Input preferred sum: ");
            int sum = sc.nextInt();
            if ((age >= 18) && (sum <= (age * 100))) {
                System.out.println("Credit approved! Congratulations!");
            } else {
                System.out.println("Credit denied!");
            }
        }
    }
}
