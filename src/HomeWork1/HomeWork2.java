package HomeWork1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {

    public static void main(String[] args) {
        int arrayCapacity = 0;
        int i, j;
        System.out.print("Please, input first number: ");
        arrayCapacity = inputIntTrue(arrayCapacity);
        System.out.print("Please, input second number: ");
        int arrayCapacitySecond = inputIntTrue(arrayCapacity);
        Random rd = new Random();
        int[][] ourArray = new int[arrayCapacity][arrayCapacitySecond];
        for (i = 0; i < arrayCapacity; i++) {
            for (j = 0; j < arrayCapacitySecond; j++) {
                ourArray[i][j] = rd.nextInt();
            }
            System.out.println(Arrays.deepToString(ourArray));
        }
        int min = getMinValue(ourArray);
        System.out.println("Minimum number in this array is " + min);
    }

    private static int getMinValue(int[][] ourArray) {
        int minValue = ourArray[0][0];
        for (int i = 0; i < ourArray.length; i++) {
            for (int j = 0; j < ourArray[0].length; j++) {
                if (ourArray[i][j] < minValue) {
                    minValue = ourArray[i][j];
                }
            }
        }
        return minValue;
    }

    private static int inputIntTrue(int arrayCapacity) throws NumberFormatException {
        boolean isValid = false;
        Scanner inputNumber = new Scanner(System.in);
        while (!isValid) {
            isValid = true;

            try {
                arrayCapacity = Integer.parseInt(inputNumber.nextLine());
                if (arrayCapacity <= 0) {
                    System.out.print("Incorrect number! Please, input positive number: ");
                    isValid = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("You can input only number! Please, input number: ");

                isValid = false;
            }
        }
        return arrayCapacity;
    }
}
