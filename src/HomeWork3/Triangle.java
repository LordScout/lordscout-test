package HomeWork3;

import java.util.Scanner;

public class Triangle implements Shape {
    double side;
    double sideA;
    double sideB;
    double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public Triangle() {
        Scanner squareInput = new Scanner(System.in);
        System.out.println("Creating Triangle");
        sideA = CheckInput.inputValueTrue(side);
        sideB = CheckInput.inputValueTrue(side);
        sideC = CheckInput.inputValueTrue(side);
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    @Override
    public double getArea() {
        double p = ((sideA + sideB + sideC) / 2);
        return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }
}
