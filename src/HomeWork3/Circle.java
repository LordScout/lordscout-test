package HomeWork3;

public class Circle implements Shape {
    private double radius;

    private final double PI = Math.PI;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
        System.out.println("Creating Circle");
        radius = CheckInput.inputValueTrue(radius);
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {

        return (PI * Math.pow(radius, 2));
    }
}
