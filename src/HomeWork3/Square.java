package HomeWork3;

public class Square implements Shape {
    private double side;

    public Square(double side) {

        this.side = side;
    }

    public Square() {
        System.out.println("Creating Square");
        side = CheckInput.inputValueTrue(side);
    }

    public void setSide(double side) {

        this.side = side;
    }

    @Override
    public double getArea() {

        return Math.pow(side, 2);
    }
}
