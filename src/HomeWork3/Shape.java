package HomeWork3;

public interface Shape {
    double getArea();

    default void print(){
        System.out.println(this.getClass().getName());
        System.out.println("Shape area is: " + this.getArea());
    }
}
