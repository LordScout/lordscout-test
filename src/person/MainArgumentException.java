package person;

public class MainArgumentException extends RuntimeException{
    public MainArgumentException(Class clazz, String field){
        super(String.format("В классе %s не верное или отсутствует поле %s", clazz, field));
        System.out.println(String.format("В классе %s не верное или отсутствует поле %s", clazz, field));
    }
}
