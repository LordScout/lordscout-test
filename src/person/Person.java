package person;

import static person.paper.Birthpaper.birthPaper;
import static person.paper.Passport.passport;
import static person.paper.StudentPaper.studentPaper;

public class Person {

    private String name;
    private String surName;
    private String thirdName; //not
    private Integer age;
    private String country;
    private String sex; //not
    public static String paper;
    private String seriesPaper;
    private String numberPaper;
    private String familyStatus; //not

    public Person(){
    }

    public String getName() {
        return name;
    }

    public Person withName(String name) {
        this.name = name;
        return this;
    }

    public String getSurName() {
        return surName;
    }

    public Person withSurName(String surName) {
        this.surName = surName;
        return this;
    }

    public String getThirdName() {
        return thirdName;
    }

    public Person withThirdName(String thirdName) {
        this.thirdName = thirdName;
        return this;
    }

    public Integer getAge()
    {
        return age;
    }

    public Person withAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Person withCountry(String country) {
        this.country = country;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public Person withSex(String sex) {
        this.sex = sex;
        return this;
    }

    public Person getPaper(String paper) {
        this.paper = paper;
        return this;
    }
    public static String setPaper(String paper){

        return Person.paper;
    }

    public Person withPaper(String paper) {

        if(age >= 18){
            paper = passport;
        } else if (age < 16){
            paper = birthPaper;
        } else if (age >= 16 && age <18){
            paper = studentPaper;
        }
        this.paper = paper;
        return this;
    }

    public String getSeriesPaper() {
        return seriesPaper;
    }

    public Person withSeriesPaper(String seriesPaper) {
        this.seriesPaper = seriesPaper;
        return this;
    }

    public String getNumberPaper() {
        return numberPaper;
    }

    public Person withNumberPaper(String numberPaper) {
        this.numberPaper = numberPaper;
        return this;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }
    public Person withFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
        return this;
    }
    public Person withFamilyStatusValidate(String familyStatus) {
        this.familyStatus = familyStatus;
        if(name == null || name.isEmpty()){
            throw new MainArgumentException(Person.class, "name");
        }
        return this;
    }
    public Person validateAll(){
        if(name == null || name.isEmpty()){
            throw new MainArgumentException(Person.class, "name");
        } else if(surName == null || surName.isEmpty()){
            throw new MainArgumentException(Person.class, "surName");
        } else if(thirdName == null || thirdName.isEmpty()){
            throw new MainArgumentException(Person.class, "thirdName");
        } else if(age == null || age < 0 || age > 120){
            throw new MainArgumentException(Person.class, "age");
        } else if(country == null || country.isEmpty()){
            throw new MainArgumentException(Person.class, "country");
        } else if(sex == null || sex.isEmpty()){
            throw new MainArgumentException(Person.class, "sex");
        } else if(paper == null || paper.isEmpty()){
            throw new MainArgumentException(Person.class, "paper");
        } else if(seriesPaper == null || seriesPaper.isEmpty()){
            throw new MainArgumentException(Person.class, "seriesPaper");
        } else if(numberPaper == null || numberPaper.isEmpty()){
            throw new MainArgumentException(Person.class, "numberPaper");
        } else if(familyStatus == null || familyStatus.isEmpty()){
            throw new MainArgumentException(Person.class, "familyStatus");
        }
        return this;
    }
//    public Person validateNSACPSN() throws Java7MultipleExceptions.FirstException, Java7MultipleExceptions.SecondException, Java7MultipleExceptions.ThirdException {
//        if(name == null || name.isEmpty()){
//            Java7MultipleExceptions.rethrow(name);
//        } else if(surName == null || surName.isEmpty()){
//            Java7MultipleExceptions.rethrow(surName);
//        }  else if(age == null || age < 0 || age > 120){
//            throw new MainArgumentException(Person.class, "age");
//        } else if(country == null || country.isEmpty()){
//            Java7MultipleExceptions.rethrow(country);
//        }  else if(paper == null || paper.isEmpty()){
//            throw new MainArgumentException(Person.class, "paper");
//        } else if(seriesPaper == null || seriesPaper.isEmpty()){
//            throw new MainArgumentException(Person.class, "seriesPaper");
//        } else if(numberPaper == null || numberPaper.isEmpty()){
//            throw new MainArgumentException(Person.class, "numberPaper");
//        }
//        return this;
//    }
}
