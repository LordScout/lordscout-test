package person;

import static person.Person.paper;

public class Main{
    public static void main(String[] args) throws MainArgumentException {
        Person person1 = new Person()
                .withName("Vasiliy")
                .withSurName("Ivanov")
                .withThirdName("Petrovich")
                .withAge(12)
                .withCountry("Russia")
                .withSex("male")
                .withPaper(paper)
                .withSeriesPaper("3232")
                .withNumberPaper("456321")
                .withFamilyStatusValidate("married")
                        .validateAll();
        System.out.println(paper);
//        Person person2 = new Person()
//
//
//                .withThirdName("Petrovich")
//                .withAge(25)
//
//                .withSex("male")
//                .withPaper("passport")
//                .withSeriesPaper("3232")
//                .withNumberPaper("456321")
//                .withFamilyStatus("married")
//                .validateNSACPSN();
    }
}
