package person.paper;

import static person.Person.paper;

public class StudentPaper implements Paper{

    public static String studentPaper = "студенческий билет";

    @Override
    public String paperType(String studentPaper) {
        return studentPaper;
    }

    @Override
    public String setPaper() {
        return paper;
    }
}
