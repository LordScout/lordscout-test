package person.paper;

import static person.Person.*;

public class Birthpaper implements Paper {

    public static String birthPaper = "свидетельство о рождении";

    @Override
    public String paperType(String birthPaper) {
        return birthPaper;
    }

    @Override
    public String setPaper() {
        return paper;
    }
}
